import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Main from "./containers/Main/Main";
import Home from "./containers/Home/Home";
import About from "./containers/About/About";
import Category from "./containers/Category/Category";
import Contact from "./containers/Contact/Contact";
import CategoriesPage from "./containers/CategoriesPage/CategoriesPage";
import Basket from "./containers/Basket/Basket";

function App() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Main}/>
          <Route path="/home" component={Home}/>
          <Route path="/about" component={About}/>
          <Route path="/category" component={Category}/>
          <Route path="/contact" component={Contact}/>
          <Route path="/categories" component={CategoriesPage}/>
          <Route path="/basket" component={Basket}/>

          <Route render={() => <h1>Not Fount</h1>}/>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
