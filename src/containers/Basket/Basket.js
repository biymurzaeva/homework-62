import React, {useState} from 'react';
import Product from "../../components/Content/Product/Product";

const Basket = ({location}) => {
	const parseProduct = () => {
		const params = new URLSearchParams(location.search);
		return Object.fromEntries(params);
	}

	const [product] = useState(parseProduct());

	console.log(product);

	return (
		<div>
			<h2>Basket</h2>
			<Product
				name={product.name}
				imageProduct={product.pic}
				price={product.price}
			/>
		</div>
	);
};

export default Basket;