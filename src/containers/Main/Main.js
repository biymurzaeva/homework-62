import React, {useState} from 'react';
import Nav from "../../components/Nav/Nav";
import './Main.css';
import Sidebar from "../../components/Sidebar/Sidebar";
import Content from "../../components/Content/Content";
import samsung from "../../Images/samsung.jpg";
import apple from "../../Images/apple.jpg";
import washMachine from "../../Images/washmachine.jpg";
import lg from "../../Images/oled.jpg";
import Modal from "../../components/UI/Modal/Modal";
import ProductInfo from "../../components/ProductInfo/ProductInfo";

const Main = ({history}) => {
	const [products] = useState([
		{name: "SAMSUNG GALAXY A51 64GB BLACK", pic: samsung, price: 20000, id: 1},
		{name: "APPLE IPHONE 11 64GB BLACK", pic: apple, price: 60000, id: 2},
		{name: "SAMSUNG WF8590NLW9DYLD", pic: washMachine, price: 68000, id: 3},
		{name: "LG OLED65BXRLB.ADKB", pic: lg, price: 180000, id: 4},
	]);
	const [showModal, setShowModal] = useState(false);

	const purchaseCancelHandler = () => {
		setShowModal(false);
	};

	const [modalChild, setModalChild] = useState({});

	const purchaseHandler = product => {
		setShowModal(true);
		setModalChild(product);
	};

	const addToBasket = () => {
		const params = new URLSearchParams(modalChild);

		history.push('/basket?' + params.toString());
	}

	return (
		<div className="container">
			<Modal
				show={showModal}
				close={purchaseCancelHandler}
			>
				<ProductInfo
					name={modalChild.name}
					imageProduct={modalChild.pic}
					price={modalChild.price}
					addToBasket={addToBasket}
				/>
			</Modal>
			<Nav/>
			<div className="sidebarContentBlock">
				<Sidebar/>
				<Content
					products={products}
					purchaseHandler={purchaseHandler}
				/>
			</div>
		</div>
	);
};

export default Main;