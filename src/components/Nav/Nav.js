import React from 'react';
import './Nav.css';
import {Link} from "react-router-dom";

const Nav = () => {
	return (
		<>
			<ul className="main-nav">
				<li><Link to="/home">Home</Link></li>
				<li><Link to="/about">About</Link></li>
				<li><Link to="/category">Category</Link></li>
				<li><Link to="/contact">Contact</Link></li>
			</ul>
		</>
	);
};

export default Nav;