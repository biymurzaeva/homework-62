import React from 'react';
import './ProductInfo.css';

const ProductInfo = props => {
	return (
		<div className="info">
			<div className="product-block">
				<div className="img-block">
					<img src={props.imageProduct} alt={props.name} className="img"/>
				</div>
				<p>{props.name}</p>
				<p>{props.price} KGZ</p>
			</div>
			<div className="description-block">
				<h5>Описание</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque earum eius iste magnam nobis!
					Consequatur dignissimos iste modi officiis qui ratione, voluptates. Eveniet odio perspiciatis ratione tempore?
					Alias expedita, rem.</p>
				<button className="red-btn" onClick={props.addToBasket}>В корзину</button>
			</div>
		</div>
	);
};

export default ProductInfo;