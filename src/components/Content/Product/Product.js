import React from 'react';
import './Product.css';

const Product = props => {
	return (
		<div className="product" onClick={props.onProductClick}>
			<div className="img-block">
				<img src={props.imageProduct} alt={props.name} className="img"/>
			</div>
			<p>{props.name}</p>
			<p>{props.price} KGZ</p>
		</div>
	);
};

export default Product;