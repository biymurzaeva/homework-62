import React from 'react';
import './Content.css';
import Product from "./Product/Product";

const Content = ({products, purchaseHandler}) => {
	return (
		<>
			<div className="content">
				{products.map(p => (
					<Product
						key={p.id}
						name={p.name}
						imageProduct={p.pic}
						price={p.price}
						onProductClick={() => purchaseHandler(p)}
					/>
				))}
			</div>
		</>
	);
};

export default Content;