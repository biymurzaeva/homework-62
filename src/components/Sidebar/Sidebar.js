import React from 'react';
import {Link} from "react-router-dom";
import './Sidebar.css';

const Sidebar = () => {
	return (
		<div className="sidebar">
			<h3>Categories: </h3>
			<ul className="categories">
				<li><Link to={{pathname: "/categories", name: "Техника для дома"}}>Техника для дома</Link></li>
				<li><Link to={{pathname: "/categories", name: "Техника для кухни"}}>Техника для кухни</Link></li>
				<li><Link to={{pathname: "/categories", name: "Смартфоны и гаджеты"}}>Смартфоны и гаджеты</Link></li>
				<li><Link to={{pathname: "/categories", name: "Ноутбуки и компьютеры"}}>Ноутбуки и компьютеры</Link></li>
				<li><Link to={{pathname: "/categories", name: "Теле и аудио техника"}}>Теле и аудио техника</Link></li>
				<li><Link to={{pathname: "/categories", name: "Встраиваемая техника"}}>Встраиваемая техника</Link></li>
			</ul>
		</div>
	);
};

export default Sidebar;